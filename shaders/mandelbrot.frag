

struct t_reel
{
  	float				i;
	float				r;
} s_reel;


int	get_manderbrot_color(t_reel a, t_reel b, t_reel c,
		int max_iter)
{
	int i;

	i = 0;
	while (++i < max_iter)
	{
		c.r = b.r * b.r - b.i * b.i + a.r;
		b.i = 2.0 * b.r * b.i + a.i;
		b.r = c.r;
		if (b.r * b.r + b.i * b.i > 4.0)
			break ;
	}
	return (i);
}
vec3 palette(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d){
    return a + b * cos(6.28318 * (c * t + d));
}

vec3			mandelbrot(float zoom, t_reel m, vec2 fragCoord)
{
	t_reel	c[3];

	int		i[3];

	float aspect = iResolution.x / iResolution.y;
		i[0] = -1;

		c[0].r = aspect * (fragCoord.x - iResolution.x * 0.5) / (iResolution.x * zoom * 0.5) + m.i;
		c[0].i = aspect * (fragCoord.y - iResolution.y * 0.5) / (iResolution.y * zoom * 0.5) + m.r;
		c[1].i = 0.0;
		c[1].r = 0.0;
    int color = get_manderbrot_color(c[0], c[1], c[2], 120);
    vec3 rgb = palette(float(color) / 120.0, vec3(0.4, 0.7, 0.4), vec3(	0.2, 0.4, 0.7), vec3(2.0, 1.0, 1.0), vec3(1.4, 0.25, 0.25));;
    return rgb;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    vec2 uv = fragCoord.xy / iResolution.xy;
    t_reel m;
    m.r = 0.0;
    m.i = -0.7;
    fragColor = vec4( mandelbrot(1.2 , m ,fragCoord),1.0);
}
