//#SHADERPIXEL shaders/buffer.frag 0
vec3 blur(vec2 fragCoord) {
    const float offset[3] = float[](0.0, 1.3846153846, 3.2307692308);
    const float weight[3] = float[](0.2270270270, 0.3162162162, 0.0702702703);
    vec4 color;
    color = texture(iChannel0, vec2(fragCoord) / iResolution.x) * weight[0];
    for (int i = 1; i < 3; i++) {
	color += texture(iChannel0,(vec2(fragCoord) + (vec2(0.0, offset[i] / iResolution.xy)))) * weight[i];
	color += texture(iChannel0, (vec2(fragCoord) - (vec2(0.0, offset[i] / iResolution.xy)))) * weight[i];
    }
    return (color.rgb);
}

float random (in vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
}

float map(float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

vec2 CRTCurveUV(vec2 uv) {
    uv = uv * 2.0 - 1.0;
    vec2 offset = abs(uv.yx) / vec2(6.0, 4.0);
    uv = uv + uv * offset * offset;
    uv = uv * 0.5 + 0.5;
    return uv;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord){
    vec2 uv = fragCoord / iResolution.xy;
    vec3 color = blur(uv);
    float displa = mod(iTime, 5.0);
    if (displa >= 0.0 && displa < 0.3) {
	vec2 displacment_uv = vec2(random(vec2(0.0, iTime)) - 0.5, 0.0);
	color = mix(color, blur((uv + displacment_uv) * 0.9), 0.4);
    }
    vec2 crtuv = CRTCurveUV(uv);

    float y = mod(floor(iTime / 10.0 + uv.y * iResolution.y / 4.0), 2.0);
    color = vec3(color * vec3(y));
    float x = mod(floor(iTime * 512.0 - uv.x * iResolution.x / 3.0), iResolution.x / 4.0);
    float limit = 5.0 * random(uv) + 5.0;
    if (x >= 0.0 && x <= limit) {
	color = mix(vec3(0.0,0.0,0.0), color, x / limit);
    }
    if (crtuv.x < 0.0 || crtuv.x > 1.0 || crtuv.y < 0.0 || crtuv.y > 1.0 ){
	color = vec3(0.0);
    }
    fragColor = vec4(color, 1.0);
}
