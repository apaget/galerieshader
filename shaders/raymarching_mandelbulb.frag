//#define SHADERTOY
const float BAILOUT = 2.0f;
const float POWER = 8.0f;
const float SCALE = 3.0f;

float distEval(vec3 pos) {
    pos /= SCALE;
    vec3 z = pos;
    float dr = 1.0;
    float r = 0.0;
    for (int n = 0; n < 10; n++) {
        r = length(z);
        if (r > BAILOUT) break ;
        float theta = acos(z.z / r);
        float phi = atan(z.y, z.x);
        dr = pow(r, POWER - 1.0) * POWER * dr + 1.0;
        float zr = pow(r, POWER);
        theta = theta * POWER;
        phi = phi * POWER;
        z = zr * vec3(sin(theta) * cos(phi), sin(phi) * sin(theta), cos(theta));
        z += pos;
    }
    return (0.5 * log(r) * r / dr) * SCALE;
}

float softshadow(in vec3 ro, in vec3 rd, float mint, float maxt, float k)
{
    float res = 1.0;
    for (float t = mint; t < maxt; ) {
        float h = distEval(ro + rd*t);
        if (h < 0.001)
            return 0.0;
        res = min(res, k * h / t);
        t += h;
    }
    return (clamp(res, 0.0, 1.0));
}

float ambientocclusion(in vec3 pos, in vec3 nor){
    float occ = 0.0;
    float sca = 1.0;
    const int samples = 5;
    for (int i = 0; i < samples; i++) {
        float hr = 0.12 * float(i) / float(samples - 1);
        vec3 aopos = nor * hr + pos;
        float dd = distEval(aopos);
        occ += -(dd - hr) * sca;
        sca *= 1.0;
    }
    return clamp(1.0 - float(samples - 1) * occ, 0.0, 1.0);
}

vec3 getNormal(vec3 pos) {
    const float EPSILON = 0.001;
    vec2 eps = vec2(0.0, EPSILON);
    vec3 normal = normalize(vec3(
                distEval(pos + eps.yxx) - distEval(pos - eps.yxx),
                distEval(pos + eps.xyx) - distEval(pos - eps.xyx),
                distEval(pos + eps.xxy) - distEval(pos - eps.xxy)));
    return (normal);
}

vec3 shade(vec3 p) {
    vec3 ambientColor = vec3((p.x, 0.9), min(p.y, 0.6), p.z);
    vec3 lightPosition = vec3(0.0, 55.0, 0.0);
    vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 normal = getNormal(p);
    vec3 lightDir = normalize(lightPosition - p);
    vec3 lightIntensity = lightColor * dot(normal, lightDir);
    float kdiffuse = max(0.0, dot(lightDir, normal));
    float specular = pow(kdiffuse, 12.0);
    float kshadow = softshadow(lightPosition, normalize(vec3(0.0, -1.0, 0.0)), 0.1, 200.0, 30.0);
    float occlusion = ambientocclusion(p, normal);
    return clamp(2.2 * occlusion * (0.2 * (ambientColor) + 0.1 * (specular * lightColor) + 0.5 * (lightColor * kdiffuse)) * kshadow, 0.0, 1.0);
}

vec3 raymarcher(vec3 rayOrigin, vec3 rayDir, inout float depth) {
    const int MAX_ITER = 100;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.003;

    float totalDist = 0.0;
    vec3 pos = rayOrigin;
    float dist = EPSILON;

    for (int i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = distEval(pos);
        totalDist += dist;
        pos += dist * rayDir;
    }
    if (dist < EPSILON) {
        depth = totalDist;
        return (shade(pos));
    }
    return (vec3(0.0));
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
#ifdef SHADERTOY
    float depth = 0.0;
    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
    vec3 eye = vec3(10.0 * sin(iTime * 0.3), 6.0 + 1.2 * sin(iTime * 0.5), 10.0 * cos(iTime * 0.3));
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    vec3 rayDirection = viewToWorld * viewDir;
#else
    float depth = zFar;
    vec3 rayDirection = normalize(dir);
#endif

    vec3 color = raymarcher(eye, rayDirection, depth);

#ifndef SHADERTOY
    if (depth < zFar) {
        float eyeHitZ = -depth * dot(cameraForward,rayDirection);
        float ndcDepth = ((zFar+zNear) + (2.0 * zFar * zNear) / eyeHitZ) / (zFar-zNear);
        gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
    } else {
        gl_FragDepth = 1.0;
    }
#endif
    fragColor = vec4(color, 1.0);
}
