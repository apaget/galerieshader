//#define SHADERTOY
const vec3 light_position = vec3(0.0, 6.0, 0.0);
const vec3 light_color = vec3(0.8, 0.8, 0.8);

float intersectSDF(float distA, float distB) {
  return max(distA, distB);
}

float unionSDF(float distA, float distB) {
  return min(distA, distB);
}

float differenceSDF(float distA, float distB) {
  return max(distA, -distB);
}

float coneSDF(vec3 pos, vec2 c){
  // c must be normalized
  float q = length(pos.xz);
  return dot(c, vec2(q, pos.y));
}

float sphereSDF(vec3 pos, float radius) {
  return (length(pos) - radius);
}

float boxSDF(vec3 p, vec3 b) {
  return length(max(abs(p) - b, 0.0));
}

float map(float x, float in_min, float in_max, float out_min, float out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float noise3D(vec3 p) {
  return fract(cos(dot(p ,vec3(12.9898,78.233,128.852))) * 43758.5453)*2.0-1.0;
}

float simplex3D(vec3 p) {

  float f3 = 1.0/3.0;
  float s = (p.x+p.y+p.z)*f3;
  int i = int(floor(p.x+s));
  int j = int(floor(p.y+s));
  int k = int(floor(p.z+s));

  float g3 = 1.0/6.0;
  float t = float((i+j+k))*g3;
  float x0 = float(i)-t;
  float y0 = float(j)-t;
  float z0 = float(k)-t;
  x0 = p.x-x0;
  y0 = p.y-y0;
  z0 = p.z-z0;

  int i1,j1,k1;
  int i2,j2,k2;

  if(x0>=y0)
  {
    if(y0>=z0){ i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; } // X Y Z order
    else if(x0>=z0){ i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; } // X Z Y order
    else { i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; }  // Z X Z order
  }
  else 
  { 
    if(y0<z0) { i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; } // Z Y X order
    else if(x0<z0) { i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; } // Y Z X order
    else { i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; } // Y X Z order
  }

  float x1 = x0 - float(i1) + g3; 
  float y1 = y0 - float(j1) + g3;
  float z1 = z0 - float(k1) + g3;
  float x2 = x0 - float(i2) + 2.0*g3; 
  float y2 = y0 - float(j2) + 2.0*g3;
  float z2 = z0 - float(k2) + 2.0*g3;
  float x3 = x0 - 1.0 + 3.0*g3; 
  float y3 = y0 - 1.0 + 3.0*g3;
  float z3 = z0 - 1.0 + 3.0*g3;	

  vec3 ijk0 = vec3(i,j,k);
  vec3 ijk1 = vec3(i+i1,j+j1,k+k1);	
  vec3 ijk2 = vec3(i+i2,j+j2,k+k2);
  vec3 ijk3 = vec3(i+1,j+1,k+1);	

  vec3 gr0 = normalize(vec3(noise3D(ijk0),noise3D(ijk0*2.01),noise3D(ijk0*2.02)));
  vec3 gr1 = normalize(vec3(noise3D(ijk1),noise3D(ijk1*2.01),noise3D(ijk1*2.02)));
  vec3 gr2 = normalize(vec3(noise3D(ijk2),noise3D(ijk2*2.01),noise3D(ijk2*2.02)));
  vec3 gr3 = normalize(vec3(noise3D(ijk3),noise3D(ijk3*2.01),noise3D(ijk3*2.02)));

  float n0 = 0.0;
  float n1 = 0.0;
  float n2 = 0.0;
  float n3 = 0.0;

  float t0 = 0.5 - x0*x0 - y0*y0 - z0*z0;
  if(t0>=0.0)
  {
    t0*=t0;
    n0 = t0 * t0 * dot(gr0, vec3(x0, y0, z0));
  }
  float t1 = 0.5 - x1*x1 - y1*y1 - z1*z1;
  if(t1>=0.0)
  {
    t1*=t1;
    n1 = t1 * t1 * dot(gr1, vec3(x1, y1, z1));
  }
  float t2 = 0.5 - x2*x2 - y2*y2 - z2*z2;
  if(t2>=0.0)
  {
    t2 *= t2;
    n2 = t2 * t2 * dot(gr2, vec3(x2, y2, z2));
  }
  float t3 = 0.5 - x3*x3 - y3*y3 - z3*z3;
  if(t3>=0.0)
  {
    t3 *= t3;
    n3 = t3 * t3 * dot(gr3, vec3(x3, y3, z3));
  }
  return 96.0*(n0+n1+n2+n3);

}


vec3 shade(vec3 pos, vec3 normal) {
  //vec3 ambientColor = vec3(cos(pos.x), sin(pos.y), tan(pos.z));
  vec3 ambientColor = vec3(1.0, 0.0, 0.0);
  vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
  vec3 lightDir = normalize(light_position - pos);
  vec3 lightIntensity = lightColor * dot(normal, lightDir);
  float kdiffuse = max(0.0, dot(lightDir, normal));
  vec3 specularD = reflect(-lightDir, normal);
  if (dot(-lightDir, normal) > 0.0) {
    specularD = vec3(0.0, 0.0, 0.0);
  }
  float specular = pow(max(0.0, dot(specularD, normal)), 2.0);
  return clamp((0.2 * (ambientColor) + 0.3 * (specular * lightColor) + 0.6 * (lightColor * kdiffuse)), 0.0, 1.0);
}

float map_translucide(vec3 pos) {
  float box = boxSDF(pos - light_position + 5.0, vec3(5.0, 5.0, 5.0));
  return box;
  return coneSDF(pos - light_position, normalize(vec2(1.0, 0.2)));
}

float map_invert_translucide(vec3 pos) {
  float box = boxSDF(pos, vec3(1000.0, 1000.0, 1000.0));
  return differenceSDF(box, map_translucide(pos));
}

float map(vec3 pos) {
  vec3 box_sphere_pos = pos + vec3(0.0 + cos(iTime) * 2.0, 0.0, 0.0 + sin(iTime) * 0.0);
  //vec3 box_sphere_pos = pos + vec3(-7.0, 0.0, 0.9);
  float box = boxSDF(box_sphere_pos, vec3(1.0, 1.0, 1.0));
  float sphere = sphereSDF(box_sphere_pos, 1.3);
  float diff = differenceSDF(box, sphere);
  return (1.0);
}

vec3 get_normal(vec3 pos) {
  const float EPSILON = 0.0001;
  vec2 eps = vec2(0.0, EPSILON);
  vec3 normal = normalize(vec3(
	map(pos + eps.yxx) - map(pos - eps.yxx),
	map(pos + eps.xyx) - map(pos - eps.xyx),
	map(pos + eps.xxy) - map(pos - eps.xxy)));
  return (normal);
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
  vec3 f = normalize(center - eye);
  vec3 s = normalize(cross(f, up));
  vec3 u = cross(s, f);
  return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
  vec2 xy = fragCoord - size / 2.0;
  float z = size.y / tan(radians(fieldOfView) / 2.0);
  return normalize(vec3(xy, -z));
}

float get_distance(vec3 ro, vec3 rd) {
  const int MAX_ITER = 15;
  const float MAX_DIST = 100.0;
  const float EPSILON = 0.01;

  vec3 pos = ro;
  float dist = EPSILON;
  for (int i = 0; i < MAX_ITER; i++) {
    if (dist < EPSILON)
      break ;
    dist = map_invert_translucide(pos);
    pos += dist * rd;
  }
  return (distance(ro, pos));
}

float raylight(vec3 ro, vec3 rd, vec3 hit_pos) {
  const int MAX_ITER = 100;
  const float MAX_DIST = 100.0;
  const float EPSILON = 0.01;

  float totalDist = 0.0;
  vec3 pos = ro;
  float dist = EPSILON;
  for (int i = 0; i < MAX_ITER; i++) {
    if (dist < EPSILON || totalDist > MAX_DIST)
      break ;
    dist = map(pos);
    totalDist += dist;

    pos += dist * rd;
  }
  if (dist < EPSILON) {
    if (distance(pos, light_position) > distance(hit_pos, light_position)) {
      float b = exp(distance(pos, light_position) - distance(hit_pos, light_position));
      return (1.0);
    } else {
      return (0.0);
    }
  } else {
    return(1.0);
  }
}

float raytranslucide(vec3 ro, vec3 rd, out float depth) {
  const int MAX_ITER = 30;
  const float EPSILON = 0.1;

  bool depth_set = true; // Compute depth until we encounter light
  depth = 0.0;
  float totalDist = 0.0;
  vec3 pos = ro + rd * EPSILON;
  float MAX_DIST = get_distance(pos, rd);
  float step_offset = float(MAX_DIST) / float(MAX_ITER);
  float dist_light = EPSILON;
  float color = 0.0;
  int hit = 0;
  for (int i = 0; i < MAX_ITER; i++) {
    pos += step_offset * rd;
    float alight = raylight(light_position, normalize(pos - light_position), pos);
    if (alight == 1.0)
      hit++;
    color += alight;
    if (depth_set && alight == 0.0) {
      depth += step_offset;
    } else {
      depth_set = false;
    }
  }
  return (float(hit) / float(MAX_ITER));
}

vec4 raymarch(vec3 ro, vec3 rd, inout float depth) {
  const int MAX_ITER = 50;
  const float MAX_DIST = 100.0;
  const float EPSILON = 0.001;

  float total_dist_scene = 0.0;
  float total_dist_light = 0.0;
  vec3 pos_scene = ro;
  vec3 pos_light = ro;
  float dist = EPSILON;
  float dist_light = EPSILON;
  bool is_light = false;
  bool is_scene = false;
  bool is_mix = false;
  float light = 0.0;
  int i;
  int j;
  for (i = 0; i < MAX_ITER; i++) {
    if (dist >= EPSILON) {
      dist = map(pos_scene);
      total_dist_scene += dist;
      pos_scene += dist * rd;
    }
    if (dist_light >= EPSILON) {
      dist_light = map_translucide(pos_light);
      total_dist_light += dist_light;
      pos_light += dist_light * rd;
    }
  }
  if (min(dist, dist_light) < EPSILON) {
    is_light = dist_light < EPSILON ? true : false;
    is_scene = dist < EPSILON ? true : false;
    is_mix = (is_light && is_scene) && total_dist_light < total_dist_scene ? true : false;
    if (is_mix) {
      float depth_offset;
      light = raytranslucide(pos_light, rd, depth_offset);
      depth = min(total_dist_light, total_dist_scene);
      /*if (light == 0.0)
	{
	light = 1.0;
	return (vec4(vec3(1.0), 1.0));
	}*/
      return (vec4(light_color * light * 0.4
	    +  0.6 * shade(pos_scene, get_normal(pos_scene)), 1.0));
    } else if (is_scene) {
      depth = total_dist_scene;
      return (vec4(0.6 * shade(pos_scene, get_normal(pos_scene)), 1.0));
    } else if (is_light) {
      float depth_offset;
      light = raytranslucide(pos_light, rd, depth_offset);
      depth = total_dist_light + depth_offset;
      if (light == 0.0) {
	return vec4(0.0);
      }
      float noise = simplex3D((pos_light / 4) + (iTime / 4.0));
      noise = map(noise, 0.0, 1.0, 0.1, 0.7);
      light = map(light, 0.0, 1.0, 0.0, 0.6);
      vec4 t = vec4(light_color , light);
      vec4 tt = vec4(light_color * noise, 0.1);
      return (mix(t, tt, 0.4));
    }
  } else {
    //depth = min(total_dist_light, total_dist_scene);
    return vec4(0.0, 0.0, 0.0, 0.0);
  }
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
#ifdef SHADERTOY
  float depth = 0.0;
  vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
  //vec3 eye = vec3(15.0 * cos(iTime * 0.5), 0.0 + 1.2 * sin(iTime * 0.5), -15.0 * sin(iTime * 0.5));
  vec3 eye = vec3(15.0, 0.0, 0.0);
  mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
  vec3 rayDirection = viewToWorld * viewDir;
#else
  float depth = zFar;
  vec3 rayDirection = normalize(dir);
#endif

  vec4 color = raymarch(eye, rayDirection, depth);

#ifndef SHADERTOY
  if (depth < zFar) {
    float eyeHitZ = -depth * dot(cameraForward,rayDirection);
    float ndcDepth = ((zFar+zNear) + (2.0 * zFar * zNear) / eyeHitZ) / (zFar-zNear);
    gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
  } else {
    gl_FragDepth = 1.0;
  }
#endif
  fragColor = vec4(color);
}
