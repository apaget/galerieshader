#define SHADERTOY

void boxFold(inout vec4 position) {
    position = clamp(position, -1.0, 1.0) * 2.0 - position;
}

float distEval(vec3 pos) {
    const float delta = 0.0;
    float dr = 2.0;
    vec4 v = vec4(0.0);
    vec4 ct = abs(vec4((pos.x + pos.y + pos.z) - delta,
                   (-pos.x - pos.y + pos.z) - delta,
                   (-pos.x + pos.y - pos.z) - delta,
                   (pos.x - pos.y - pos.z) - delta));
    float radius = 0.0;
    for (int n = 0; n < 20; n++) {
        boxFold(v);
        radius = dot(v, v);
        float c = clamp(max(0.25 / radius, 0.25), 0.0, 1.0) / 0.25;
        v *= c;
        dr /= c;
       	v = v * 2.0 + ct;
       	dr /= 2.0;
        if (radius > 3600.0) break ;   
    }
    return (dr * sqrt(radius));
}

vec3 raymarcher(vec3 rayOrigin, vec3 rayDir, inout float depth) {
    const int MAX_ITER = 100;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.001;

    float totalDist = 0.0;
    vec3 pos = rayOrigin;
    float dist = EPSILON;

    // TODO: try to raytrace AABB to discard rays early
    for (int i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = distEval(pos);
        totalDist += dist;
        pos += dist * rayDir;
    }
    if (dist < EPSILON) {
        vec2 eps = vec2(0.0, EPSILON);
        vec3 normal = normalize(vec3(
                    distEval(pos + eps.yxx) - distEval(pos - eps.yxx),
                    distEval(pos + eps.xyx) - distEval(pos - eps.xyx),
                    distEval(pos + eps.xxy) - distEval(pos - eps.xxy)));
        float diffuse = max(0.0, dot(-rayDir, normal));
        float specular = pow(diffuse, 32.0);
        vec3 color = vec3(diffuse + specular);
        depth = totalDist;
        return (color);
    }
    return (vec3(0.0));
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
#ifdef SHADERTOY
    float depth = 0.0;
    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
    vec3 eye = vec3(20.0 * sin(iTime * 0.3), 0.0 + 1.2 * sin(iTime * 0.5), 20.0 * cos(iTime * 0.3));
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    vec3 rayDirection = viewToWorld * viewDir;
#else
    float depth = zFar;
    vec3 rayDirection = normalize(dir);
#endif

    vec3 color = raymarcher(eye, rayDirection, depth);

#ifndef SHADERTOY
    if (depth < zFar) {
        float eyeHitZ = -depth * dot(cameraForward,rayDirection);
        float ndcDepth = ((zFar+zNear) + (2.0 * zFar * zNear) / eyeHitZ) / (zFar-zNear);
        gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
    } else {
        gl_FragDepth = 1.0;
    }
#endif
    fragColor = vec4(color, 1.0);
}
