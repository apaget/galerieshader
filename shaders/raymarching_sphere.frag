float sphere(vec3 pos, float radius) {
    return (length(pos) - radius);
}

float distfunc(vec3 pos) {
    pos.xy = mod((pos.xy),1.0)-vec3(0.5); // instance on xy-plane
  return length(pos)-0.3;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    vec3 cameraOrigin = vec3(2.0, 2.0, 2.0);
    vec3 cameraTarget = vec3(0.0, 0.0, 0.0);
    vec3 upDirection = vec3(0.0, 1.0, 0.0);
    vec3 cameraDir = normalize(cameraTarget - cameraOrigin);
    vec3 cameraRight = normalize(cross(upDirection, cameraOrigin));
    vec3 cameraUp = cross(cameraDir, cameraRight);
    vec2 screenPos = -1.0 + 2.0 * fragCoord.xy / iResolution.xy;
    screenPos.x *= iResolution.x / iResolution.y;
    vec3 rayDir = normalize(cameraRight * screenPos.x + cameraUp * screenPos.y + cameraDir);

    const int MAX_ITER = 100;
    const float MAX_DIST = 20.0;
    const float EPSILON = 0.001;

    float totalDist = 0.0;
    vec3 pos = cameraOrigin;
    float dist = EPSILON;

    for (int i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = distfunc(pos);
        totalDist += dist;
        pos += dist * rayDir;
    }
    if (dist < EPSILON) {
        vec2 eps = vec2(0.0, EPSILON);
        vec3 normal = normalize(vec3(
                    distfunc(pos + eps.yxx) - distfunc(pos - eps.yxx),
                    distfunc(pos + eps.xyx) - distfunc(pos - eps.xyx),
                    distfunc(pos + eps.xxy) - distfunc(pos - eps.xxy)));
        float diffuse = max(0.0, dot(-rayDir, normal));
        float specular = pow(diffuse, 32.0);
        vec3 color = vec3(diffuse + specular);
        fragColor = vec4(color, 1.0);
    } else {
        fragColor = vec4(0.0f);   
    }
}
