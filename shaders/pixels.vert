#version 410 core
layout (location = 0) in vec3 vertex_pos;
layout (location = 1) in vec2 vertex_uv;
layout (location = 2) in vec3 vertex_normal;

out vec3 frag_pos;
out vec2 frag_uv;
out vec3 frag_normal;

uniform mat4 MVP;
uniform mat4 M;

void main() {
    gl_Position = MVP * vec4(vertex_pos, 1.0f);
    frag_pos = vec3(M * vec4(vertex_pos, 1.0f));
    frag_uv = vertex_uv;

    frag_normal = mat3(transpose(inverse(M))) * vertex_normal;  
}
