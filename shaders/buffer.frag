#define SHADERTOY
const float SCALE = 3.0f;
const int ITER = 5;
const float SCALING = 2.0;

float map(float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

vec3 palette(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d){
    return a + b * cos(6.28318 * (c * t + d));
}

float distEval(vec3 pos) {
    pos /= SCALING;
    mat3 matRot2 = mat3(1.0);
    float dr = 1.0;
    vec3 offset = vec3(1.0f);
    for (int n = 0; n < ITER; n++) {
        pos = abs(pos);
        if (pos.x - pos.y < 0.0) {
            float tmp = pos.y;
            pos.y = pos.x;
            pos.x = tmp;
        }
        if (pos.x - pos.z < 0.0) {
            float tmp = pos.z;
            pos.z = pos.x;
            pos.x = tmp;
        }
        if (pos.y - pos.z < 0.0) {
            float tmp = pos.z;
            pos.z = pos.y;
            pos.y = tmp;
        }
        pos.z -= 0.5 * offset.z * (SCALE - 1.0) / SCALE;
        pos.z = -abs(-pos.z);
        pos.z += 0.5 * offset.z * (SCALE - 1.0) / SCALE;
        pos.x = SCALE * pos.x - offset.x * (SCALE - 1.0);
        pos.y = SCALE * pos.y - offset.y * (SCALE - 1.0);
        pos.z = SCALE * pos.z;
    }
    vec3 d = abs(pos) - vec3(1.0);
    float dis = min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));

    dis *= pow(SCALE, float(-ITER));
    return (dis * SCALING);
}

float softshadow(in vec3 ro, in vec3 rd, float mint, float maxt, float k){
    float res = 1.0;
    for (float t = mint; t < maxt;) {
        float h = distEval(ro + rd*t);
        if(h < 0.001)
            return 0.0;
        res = min( res, k * h / t );
        t += h;
    }
    return (clamp(res, 0.0, 1.0));
}

float ambientocclusion(in vec3 pos, in vec3 nor){
    float occ = 0.0;
    float sca = 1.0;
    const int samples = 5;
    for (int i = 0; i < samples; i++) {
        float hr = 0.12 * float(i) / float(samples - 1);
        vec3 aopos = nor * hr + pos;
        float dd = distEval(aopos);
        occ += -(dd - hr) * sca;
        sca *= 1.0;
    }
    return clamp(1.0 - float(samples - 1) * occ, 0.0, 1.0);
}

vec3 getNormal(vec3 pos) {
    const float EPSILON = 0.001;
    vec2 eps = vec2(0.0, EPSILON);
    vec3 normal = normalize(vec3(
                distEval(pos + eps.yxx) - distEval(pos - eps.yxx),
                distEval(pos + eps.xyx) - distEval(pos - eps.xyx),
                distEval(pos + eps.xxy) - distEval(pos - eps.xxy)));
    return (normal);
}

vec3 shade(vec3 p, float iter) {
    vec3 ambientColor = palette(iter, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,1.0,1.0),vec3(0.0,0.10,0.20));
    vec3 lightPosition = vec3(0.0, 30.0, 0.0);
    vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 normal = getNormal(p);
    vec3 lightDir = normalize(lightPosition - p);
    vec3 lightIntensity = lightColor * dot(normal, lightDir);
    float kdiffuse = max(0.0, dot(lightDir, normal));
    float specular = pow(kdiffuse, 12.0);
    float kshadow = softshadow(lightPosition, normalize(vec3(0.0, -1.0, 0.0)), 0.1, 200.0, 30.0);
    float occlusion = ambientocclusion(p, normal);

    return clamp(ambientColor * occlusion + (0.2 * (ambientColor) + 0.1 * (specular * lightColor) + 0.5 * (lightColor * kdiffuse)) * kshadow, 0.0, 1.0);
}

vec3 raymarcher(vec3 rayOrigin, vec3 rayDir, inout float depth) {
    const int MAX_ITER = 100;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.01;

    float totalDist = 0.0;
    vec3 pos = rayOrigin;
    float dist = EPSILON;
    int i;
    for (i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = distEval(pos);
        totalDist += dist;
        pos += dist * rayDir;
    }
    if (dist < EPSILON) {
        depth = totalDist;
        return (shade(pos, clamp(map(float(i), 0.0, float(MAX_ITER / 2),0.0, 1.0), 0.0, 0.4)));
    }
    return (vec3(0.7));
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
#ifdef SHADERTOY
    float depth = 0.0;
    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
    vec3 eye = vec3(10.0 * sin(iTime * 0.3), 6.0 + 1.2 * sin(iTime * 0.5), 15.0 * cos(iTime * 0.3));
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    vec3 rayDirection = viewToWorld * viewDir;
#else
    float depth = zFar;
    vec3 rayDirection = normalize(dir);
#endif

    vec3 color = raymarcher(eye, rayDirection, depth);

#ifndef SHADERTOY
    if (depth < zFar) {
        float eyeHitZ = -depth * dot(cameraForward,rayDirection);
        float ndcDepth = ((zFar+zNear) + (2.0 * zFar * zNear) / eyeHitZ) / (zFar-zNear);
        gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
    } else {
        gl_FragDepth = 1.0;
    }
#endif
    fragColor = vec4(color, 1.0);
}
