#version 410 core

in vec3 frag_pos;
in vec2 frag_uv;
in vec3 frag_normal;

out vec4 frag_color;

uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;

uniform vec3 camPos;

void main() {
    vec3 light_dir = normalize(vec3(0.2, -1.0, 0.0));
    vec3 ambient_color = vec3(texture(iChannel0, frag_uv));
    vec3 light_color = vec3(1.0, 1.0, 1.0);
    vec3 norm = normalize(frag_normal);
    float kdiff = max(0.0, dot(norm, light_dir));
    
    vec3 view_dir = normalize(camPos - frag_pos);
    vec3 r = reflect(-light_dir, norm);
    float dotr = max(0.0, dot(-r, view_dir));
    float kspec = pow(dotr, 6.0);

    vec3 color = 0.2 * (kdiff * light_color) + 0.5 * ambient_color + 0.3 * vec3(1.0, 1.0, 1.0) * kspec;
    frag_color = vec4(color, 1.0);
}
