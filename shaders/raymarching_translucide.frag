//#define SHADERTOY

vec3 palette(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d){
    return a + b * cos(6.28318 * (c * t + d));
}

float map(float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float intersectSDF(float distA, float distB) {
    return max(distA, distB);
}

float unionSDF(float distA, float distB) {
    return min(distA, distB);
}

float differenceSDF(float distA, float distB) {
    return max(distA, -distB);
}

float sphereSDF(vec3 pos, float radius) {
    return (length(pos) - radius);
}

float boxSDF(vec3 p, vec3 b) {
    return length(max(abs(p) - b, 0.0));
}

vec3 shade(vec3 pos, vec3 normal, float iter) {
    vec3 ambientColor = palette(iter * 3.0, vec3(0.5, 0.5, 0.5), vec3(0.5, 0.5, 0.5), vec3(1.0, 1.0, 0.5), vec3(0.80, 0.90, 0.30));
    vec3 lightPosition = vec3(30.0 * sin(iTime * 1.0), 5.0, 30.0 * cos(iTime * 1.0));
    vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 lightDir = normalize(lightPosition - pos);
    vec3 lightIntensity = lightColor * dot(normal, lightDir);
    float kdiffuse = max(0.0, dot(lightDir, normal));
    vec3 specularD = reflect(-lightDir, normal);
    float specular = pow(max(0.0, dot(specularD, normal)), 2.0);
    return clamp((0.2 * (ambientColor) + 0.3 * (specular * lightColor) + 0.6 * (lightColor * kdiffuse)), 0.0, 1.0);
}

float map(vec3 pos) {
    float scale = 4.0;
    pos /= scale;
    float box = boxSDF(pos, vec3(1.0, 1.0, 1.0));
    float sphere = sphereSDF(pos, 1.3);
    float diff = differenceSDF(box, sphere);
    return (diff * scale);
}

vec3 get_normal(vec3 pos) {
    const float EPSILON = 0.0001;
    vec2 eps = vec2(0.0, EPSILON);
    vec3 normal = normalize(vec3(
                map(pos + eps.yxx) - map(pos - eps.yxx),
                map(pos + eps.xyx) - map(pos - eps.xyx),
                map(pos + eps.xxy) - map(pos - eps.xxy)));
    return (normal);
}

vec3 raymarch(vec3 ro, vec3 rd, inout float depth) {
    const int MAX_ITER = 100;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.001;

    float totalDist = 0.0;
    vec3 pos = ro;
    float dist = EPSILON;

    int i;
    for (i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = map(pos);
        totalDist += dist;
        pos += dist * rd;
    }
    if (dist < EPSILON) {
        depth = totalDist;
        return (shade(pos, get_normal(pos), map(float(i), 0.0, float(MAX_ITER),0.0, 1.0)));
    } else {
        return (vec3(0.0));
    }
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
#ifdef SHADERTOY
    float depth = 0.0;
    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
    //vec3 eye = vec3(15.0 * sin(iTime * 0.3), 6.0 + 1.2 * sin(iTime * 0.5), 15.0 * cos(iTime * 0.3));
    vec3 eye = vec3(15.0 * sin(iTime * 0.0), 6.0 + 1.2 * sin(iTime * 0.0), 15.0 * cos(iTime * 0.0));
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    vec3 rayDirection = viewToWorld * viewDir;
#else
    float depth = zFar;
    vec3 rayDirection = normalize(dir);
#endif

    vec3 color = raymarch(eye, rayDirection, depth);

#ifndef SHADERTOY
    if (depth < zFar) {
        float eyeHitZ = -depth * dot(cameraForward,rayDirection);
        float ndcDepth = ((zFar+zNear) + (2.0 * zFar * zNear) / eyeHitZ) / (zFar-zNear);
        gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
    } else {
        gl_FragDepth = 1.0;
    }
#endif
    fragColor = vec4(color, 0.6);
}
