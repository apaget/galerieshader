#define SHADERTOY
const vec3 light_position = vec3(0.0, 6.0, 0.0);

float intersectSDF(float distA, float distB) {
    return max(distA, distB);
}

float unionSDF(float distA, float distB) {
    return min(distA, distB);
}

float differenceSDF(float distA, float distB) {
    return max(distA, -distB);
}

float sphereSDF(vec3 pos, float radius) {
    return (length(pos) - radius);
}

float boxSDF(vec3 p, vec3 b) {
    return length(max(abs(p) - b, 0.0));
}

float map(float x, float in_min, float in_max, float out_min, float out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

vec3 shade(vec3 pos, vec3 normal) {
    //vec3 ambientColor = vec3(cos(pos.x), sin(pos.y), tan(pos.z));
    vec3 ambientColor = vec3(1.0, 0.0, 0.0);
    vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 lightDir = normalize(light_position - pos);
    vec3 lightIntensity = lightColor * dot(normal, lightDir);
    float kdiffuse = max(0.0, dot(lightDir, normal));
    vec3 specularD = reflect(-lightDir, normal);
    float specular = pow(max(0.0, dot(specularD, normal)), 2.0);
    return clamp((0.2 * (ambientColor) + 0.3 * (specular * lightColor) + 0.6 * (lightColor * kdiffuse)), 0.0, 1.0);
}

float map(vec3 pos) {
    float box = boxSDF(pos, vec3(1.0, 1.0, 1.0));
    float sphere = sphereSDF(pos + vec3(0.0, 0.0, sin(iTime)), 1.3);
    float diff = differenceSDF(box, sphere);
    return (sphere);
}

vec3 get_normal(vec3 pos) {
    const float EPSILON = 0.0001;
    vec2 eps = vec2(0.0, EPSILON);
    vec3 normal = normalize(vec3(
                map(pos + eps.yxx) - map(pos - eps.yxx),
                map(pos + eps.xyx) - map(pos - eps.xyx),
                map(pos + eps.xxy) - map(pos - eps.xxy)));
    return (normal);
}

mat4 orthoMatrix(float left, float right, float top, float bottom, float n, float f)
{
   	mat4 m = mat4(vec4(2.0 / right - left, 0.0, 0.0, 0.0),
           vec4(0.0, 2.0 / top - bottom, 0.0, 0.0),
          	vec4(0.0, 0.0, -2.0 / (f - n), 0.0),
            vec4(-(right  + left) / (right - left), -(top + bottom) / (top - bottom), -(f + n) / (f - n), 1.0));
	return (m);
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

vec3 raylight(vec3 ro, vec3 rd, vec3 hit_pos) {
    const int MAX_ITER = 50;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.01;

    float totalDist = 0.0;
    vec3 pos = ro;
    float dist = EPSILON;
    for (int i = 0; i < MAX_ITER; i++) {
        if (dist < EPSILON || totalDist > MAX_DIST)
            break ;
        dist = map(pos);
        totalDist += dist;
        pos += dist * rd;
    }
    if (dot(rd, normalize(vec3(0.0) - light_position)) < cos(radians(10.0)))
       return (vec3(0.0));
    if (dist < EPSILON) {
      	if (distance(pos, light_position) > distance(hit_pos, light_position)) {
     		return (vec3(1.0));
    	} else {
     		return (vec3(0.0));
    	}
    } else {
     	return(vec3(1.0));
    }
}

vec3 raymarch(vec3 ro, vec3 rd, inout float depth) {
    const int MAX_ITER = 50;
    const float MAX_DIST = 100.0;
    const float EPSILON = 0.001;

    float totalDist = 0.0;
    vec3 pos = ro;
    float dist = EPSILON;
	vec3 color = vec3(0.0);
    vec3 lightvec;
    int i;
    int j;
    //if (dot(rd, normalize(vec3(0.0) - light_position)) < cos(radians(30.0)))
    //return (vec3(0.0));
    for (i = 0; i < MAX_ITER; i++) {
        /*if (dist < EPSILON || totalDist > MAX_DIST)
            break ; */
        dist = map(pos);
        totalDist += dist;
        pos += dist * rd;
        lightvec = raylight(light_position, normalize(pos - light_position), pos);
        color += lightvec;
        if (lightvec.x != 0.0)
        	j++;
    }
    color = vec3(map(float(j), 0.0, float(MAX_ITER), 0.0, 0.7));
    //color /= float(i);

    //color = vec3(map(float(j), 0.0, 10.0, 0.0, 0.6));

    color = smoothstep(0.0, 0.5, color);
    //color /= float(i);
    //color = vec3(smoothstep(0.0, 0.4, color.x));
    if (dist < EPSILON) {
        //color /= float(i);
        depth = totalDist;
        return (mix(color, shade(pos, get_normal(pos)), 0.7));
    } else {
        lightvec = raylight(light_position, normalize(pos - light_position), pos);
        //color += lightvec;
        return (color);
    }
}

vec3 raylight(vec3 ro, vec3 rd, inout float depth) {
    const int STEP = 10;
	mat4 ortho = orthoMatrix(-10.0, 10.0, -10.0, 10.0, 1.0, 7.5);
    mat3 view = viewMatrix(light_position, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    mat3 lightProj = mat3(ortho) * view;

	return (vec3(0.0));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    float depth = 1000.0;
#ifdef SHADERTOY
    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
    vec3 eye = vec3(20.0 * sin(iTime * 0.5), 6.0 + 1.2 * sin(iTime * 0.5), 14.0 * cos(iTime * 0.5));
    //vec3 eye = vec3(20.0, 0.0, 0.0);
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    vec3 rayDirection = viewToWorld * viewDir;
#else
    vec3 rayDirection = normalize(dir);
#endif

    vec3 color = raymarch(eye, rayDirection, depth);

#ifndef SHADERTOY
    float zFar = 1000.0;
    float zNear = 0.1;
    float eyeHitZ = -depth * dot(cameraForward,rayDirection);
    float ndcDepth = ((zFar+zNear) + (2.0*zFar*zNear) / eyeHitZ) / (zFar-zNear);
    gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
#endif
    fragColor = vec4(color, 0.6);
}
