//#define SHADERTOY

vec3 palette(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d){
    return a + b * cos(6.28318 * (c * t + d));
}

float random (in vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898, 78.233)))* 43758.5453123);
}

// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(a, b, u.x) +
        (c - a)* u.y * (1.0 - u.x) +
        (d - b) * u.x * u.y;
}

// Fractional Brown noise https://thebookofshaders.com/13/
float fbm (in vec2 st) {
    //st /= 1.7;
    st *= 0.4;
    const int OCTAVES = 2;
    float value = 0.0;
    float amplitude = .5;
    float frequency = 0.;
    for (int i = 0; i < OCTAVES; i++) {
        value += amplitude * noise(st);
        st *= 3.;
        amplitude *= .7;
    }
    return value;
}

vec3 getNormal(vec3 p) {
    const float eps = 0.1;
    vec3 n = vec3(fbm(vec2(p.x - eps, p.z)) - fbm(vec2(p.x + eps, p.z)),
            0.5 * eps,
            fbm(vec2(p.x, p.z - eps)) - fbm(vec2(p.x, p.z + eps)));
    return normalize(n);
}

vec3 material(vec3 pos) {
    if (pos.y < 0.25)
        return (vec3(0.0, 0.0, 1.0));
    else
        return (palette(pos.y / 3.0, vec3(0.5, 0.5, 0.5), vec3(0.5, 0.5, 0.5), vec3(1.0, 1.0, 0.5), vec3(0.80, 0.90, 0.30)));
}

vec3 applyFog(in vec3 rgb, in float distance, vec3 rd, vec3 lightdir){
    float fogAmount = 1.0 - exp(-distance * 0.015);
    float sunAmount = max(0.0, dot(rd, lightdir));
    vec3 sunColor = vec3(1.0, 0.9, 0.7);
    vec3 fogColor  = mix(vec3(0.5,0.6,0.7), sunColor, pow(sunAmount, 32.0));
    //fogColor = mix(rgb, fogColor, fogAmount);
    return mix(rgb, fogColor, fogAmount);
}

vec3 shade(vec3 p, vec3 n, vec3 lightPosition) {
    vec3 ambientColor = material(p);
    vec3 lightColor = vec3(1.0, 0.9, 0.7);
    vec3 lightDir = normalize(lightPosition - p);
    vec3 lightIntensity = lightColor * dot(n, lightDir);
    float kdiffuse = max(0.0, dot(lightDir, n));
    float kspecular = pow(kdiffuse, 12.0);
    return clamp((0.1 * (ambientColor)  + 0.4 * (lightColor * kdiffuse) + 0.05 * (lightColor * kspecular)), 0.0, 1.0);
}

vec3 terrainColor(vec3 ro, vec3 rd, float t) {
    vec3 lightPosition = vec3(-20.0, 5.0, 1.5);
    vec3 p = ro + rd * t;
    vec3 n = getNormal(p);
    vec3 s = shade(p, n, lightPosition);
    return (applyFog(s, t, rd, normalize(lightPosition - ro)));
}

vec3 raymarcher(vec3 ro, vec3 rd, vec2 uv) {
    const float MAX_DIST = 50.0;
    float dt = 0.1;
    float lh = 0.0f;
    float ly = 0.0f;
    vec3 pos = ro;
    float dist = 1000.0;
    for (float t = 0.1; t < MAX_DIST; t += dt) {
        vec3 p = ro + rd * t;
        float height = fbm(vec2(p.x, p.z));
        if (p.y < height) {
            dist = t - dt + dt * (lh - ly) / (p.y - ly - height + lh);
            break ;
            return (terrainColor(ro, rd, dist));
        }
        dt = 0.05f * t;
        lh = height;
        ly = p.y;
    }
    return (terrainColor(ro, rd, dist));
    return (vec3(0.0));
}

mat3 rotate(vec3 angles) {
    vec3 c = cos(angles);
    vec3 s = sin(angles);

    mat3 rotX = mat3(1.0, 0.0, 0.0, 0.0,c.x,s.x, 0.0,-s.x, c.x);
    mat3 rotY = mat3(c.y, 0.0, -s.y, 0.0,1.0,0.0, s.y, 0.0, c.y);
    mat3 rotZ = mat3(c.z, s.z, 0.0, -s.z,c.z,0.0, 0.0, 0.0, 1.0);

    return rotX * rotY * rotZ;
}

mat3 viewMatrix(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 rayDir(float fieldOfView, vec2 size, vec2 fragCoord) {
    vec2 xy = fragCoord - size / 2.0;
    float z = size.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    vec2 uv = fragCoord.xy / iResolution.xy;

    vec3 viewDir = rayDir(45.0, iResolution.xy, fragCoord);
#ifdef SHADERTOY
    //vec3 eye = vec3(10.0 * sin(iTime * 0.3), 0.5 + 0.0 * sin(iTime * 0.0), 10.0 * cos(iTime * 0.3));
    vec3 eye = vec3(0.0, 0.5, -10.0);
    mat3 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
#else
    vec3 camView = normalize(camPos - billboardPos);
    mat3 viewToWorld = viewMatrix(camPos, camPos + camView, vec3(0.0, 1.0, 0.0));
    float depth = zFar;
#endif

    vec3 worldDir = viewToWorld * viewDir;
    vec3 color = raymarcher(vec3(0.0, 1.0, 0.0), worldDir, uv);
    color = pow(color, vec3(1.0 / 2.2));
    fragColor = vec4(color,1.0);
}
