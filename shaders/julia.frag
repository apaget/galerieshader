const int MAX_ITER = 40;

struct complex {
    float i;
    float r;
};

float map(float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

vec3 palette(in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d){
    return a + b * cos(6.28318 * (c * t + d));
}

int	get_iter(complex z, complex c, int max_iter) {
    complex old;
    int i = 0;

    for (i = 0; i < max_iter; i++){
        old = z;
        z.r = old.r * old.r - old.i * old.i + c.r;
        z.i = 2.0 * old.r * old.i + c.i;
        if (z.r * z.r + z.i * z.i > 4.0)
            break ;
    }
    return (i);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord){
    vec2 uv = fragCoord/iResolution.xy;

    complex z;
    z.r = iResolution.x / iResolution.y * (fragCoord.x - (iResolution.x * 0.5)) / (iResolution.x * 0.5);
    z.i = iResolution.x / iResolution.y * (fragCoord.y - (iResolution.y * 0.5)) / (iResolution.y * 0.5);

    complex c;
    //c.r = iMouse.x / iResolution.x;
    //c.i = iMouse.y / iResolution.y;
	c.r = clamp(billboardPos.x  - camPos.x * 2.0, -1.0, 1.0);
	c.i = clamp((billboardPos.z  - camPos.z) * 0.05, -1.0, 1.0);


    int iter = get_iter(z, c, MAX_ITER);
    float i = map(float(iter), 0.0, float(MAX_ITER), 0.0, 1.0);

    vec3 color = palette(i, vec3(0.5, 0.5, 0.5), vec3(0.5, 0.5, 0.5), vec3(1.0, 1.0, 0.5), vec3(0.80, 0.90, 0.30));
    fragColor = vec4(color, 1.0);
}
