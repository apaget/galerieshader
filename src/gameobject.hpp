#pragma once
#include <array>
#include "camera.hpp"
#include "env.hpp"
#include "renderer.hpp"

class GameObject;

struct Transform {
  glm::vec3 position = {0.0f, 0.0f, 0.0f};
  glm::vec3 rotation = {0.0f, 0.0f, 0.0f};
  glm::vec3 scale = {1.0f, 1.0f, 1.0f};

  Transform() {
    this->_old_position = position;
    this->_old_rotation = rotation;
    this->_old_scale = scale;
    this->_model = glm::mat4(1.0f);
  }

  glm::mat4 toModelMatrix() {
    // only update when at least one attribute has changed
    if (position != _old_position || rotation != _old_rotation ||
        scale != _old_scale) {
      glm::mat4 mat_translation = glm::translate(position);
      glm::mat4 mat_rotation =
          glm::eulerAngleYXZ(rotation.y, rotation.x, rotation.z);
      glm::mat4 mat_scale = glm::scale(scale);
      this->_model = mat_translation * mat_rotation * mat_scale;
      this->_old_position = position;
      this->_old_rotation = rotation;
      this->_old_scale = scale;
    }
    return (this->_model);
  }

 private:
  glm::mat4 _model;
  glm::vec3 _old_position;
  glm::vec3 _old_rotation;
  glm::vec3 _old_scale;
};

class GameObject {
 public:
  GameObject(Shader* shader, VAO* vao,
             glm::vec3 pos = glm::vec3(0.0f, 0.0f, 0.0f),
             glm::vec3 rot = glm::vec3(0.0f, 0.0f, 0.0f),
             glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f));
  GameObject(GameObject const& src);
  virtual ~GameObject(void);
  GameObject& operator=(GameObject const& rhs);
  const RenderAttrib getRenderAttrib() const;
  void update(InputHandler& inputHandler, const Camera& camera);
  void add(glm::mat4 transform);
  void setTransform(glm::vec3 pos = glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3 rot = glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f));
  void setChannel0(Texture* texture);
  void setChannel1(Texture* texture);
  void setChannel2(Texture* texture);
  void setChannel3(Texture* texture);
  void setBillboard(bool is_billboard);
  Transform transform;
  glm::vec3 positionRelative = {0.0f, 0.0f, 0.0f};

 private:
  GameObject(void);
  RenderAttrib _renderAttrib;
  bool _billboard;
};
