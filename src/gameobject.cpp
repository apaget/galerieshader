#include "gameobject.hpp"

GameObject::GameObject(void){};

GameObject::GameObject(Shader* shader, VAO* vao, glm::vec3 pos, glm::vec3 rot,
                       glm::vec3 scale)
    : _billboard(false) {
  this->transform.position = pos;
  this->transform.rotation = rot;
  this->transform.scale = scale;
  this->_renderAttrib.shader = shader;
  this->_renderAttrib.vao = vao;
  this->_renderAttrib.transforms.push_back(glm::mat4(1.0f));
  this->_renderAttrib.iChannel0 = nullptr;
  this->_renderAttrib.iChannel1 = nullptr;
  this->_renderAttrib.iChannel2 = nullptr;
  this->_renderAttrib.iChannel3 = nullptr;
}

GameObject::GameObject(GameObject const& src) { *this = src; }

GameObject::~GameObject(void) {}

GameObject& GameObject::operator=(GameObject const& rhs) {
  if (this != &rhs) {
    this->_renderAttrib = rhs._renderAttrib;
    this->_renderAttrib.shader = rhs._renderAttrib.shader;
    this->_renderAttrib.vao = rhs._renderAttrib.vao;
    this->_renderAttrib.transforms = rhs._renderAttrib.transforms;
    this->_renderAttrib.iChannel0 = rhs._renderAttrib.iChannel0;
    this->_renderAttrib.iChannel1 = rhs._renderAttrib.iChannel1;
    this->_renderAttrib.iChannel2 = rhs._renderAttrib.iChannel2;
    this->_renderAttrib.iChannel3 = rhs._renderAttrib.iChannel3;
    this->transform.position = rhs.transform.position;
    this->transform.rotation = rhs.transform.rotation;
    this->transform.scale = rhs.transform.scale;
    this->positionRelative = rhs.positionRelative;
  }
  return (*this);
}

glm::mat4 get_billboard(glm::vec3 position, glm::vec3 cameraPos,
                        glm::vec3 cameraUp) {
  glm::vec3 look = glm::normalize(cameraPos - position);
  glm::vec3 right = cross(cameraUp, look);
  glm::vec3 up2 = cross(look, right);
  glm::mat4 transform;
  transform[0] = glm::vec4(right, 0);
  transform[1] = glm::vec4(up2, 0);
  transform[2] = glm::vec4(look, 0);
  transform[3] = glm::vec4(position, 1.0);
  return transform;
}

void GameObject::update(InputHandler& inputHandler, const Camera& camera) {
  glm::vec3 oldPosition = this->transform.position;
  if (this->_billboard) {
    glm::mat4 billboard_matrix =
        get_billboard(this->transform.position, camera.pos, camera.up);
    glm::mat4 scale = glm::scale(transform.scale);
    // glm::mat4 scale = glm::scale(glm::vec3(1.0f));
    this->_renderAttrib.transforms[0] = billboard_matrix * scale;
    this->_renderAttrib.billboardPos = this->transform.position;
    this->_renderAttrib.billboard = transform.toModelMatrix();
    this->_renderAttrib.transforms[0] = transform.toModelMatrix();
  } else {
    this->_renderAttrib.transforms[0] = transform.toModelMatrix();
  }
  this->_renderAttrib.billboardPos = this->transform.position;
  this->positionRelative = this->transform.position - oldPosition;
}

const RenderAttrib GameObject::getRenderAttrib() const {
  return (this->_renderAttrib);
}

void GameObject::setChannel0(Texture* texture) {
  this->_renderAttrib.iChannel0 = texture;
}
void GameObject::setChannel1(Texture* texture) {
  this->_renderAttrib.iChannel1 = texture;
}
void GameObject::setChannel2(Texture* texture) {
  this->_renderAttrib.iChannel2 = texture;
}
void GameObject::setChannel3(Texture* texture) {
  this->_renderAttrib.iChannel3 = texture;
}
void GameObject::setBillboard(bool is_billboard) {
  this->_billboard = is_billboard;
}
