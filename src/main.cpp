#include <list>
#include "env.hpp"
#include "model.hpp"
#include "pixels.hpp"
#include "renderer.hpp"
#include "scene.hpp"
#include "shader.hpp"

int main(int argc, char **argv) {
  Env env(1280, 720);
  Renderer renderer(env.width, env.height);
  Scene scene(&renderer);
  renderer.uniforms.iResolution = {static_cast<float>(env.width),
                                   static_cast<float>(env.height), 0.0f};
  renderer.uniforms.iSampleRate = 44100.0f;
  renderer.loadCubeMap("shaders/skybox.vert", "shaders/skybox.frag",
                       {"models/skybox/right.jpg", "models/skybox/left.jpg",
                        "models/skybox/top.jpg", "models/skybox/bottom.jpg",
                        "models/skybox/back.jpg", "models/skybox/front.jpg"});

  while (!glfwWindowShouldClose(env.window)) {
    env.update();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glfwPollEvents();

    scene.update(env.inputHandler, env.getDeltaTime());
    renderer.uniforms.iTime = env.getAbsoluteTime();
    renderer.uniforms.iTimeDelta = env.getDeltaTime();
    renderer.uniforms.iFrame = env.getFrame();
    scene.draw();
    glfwSwapBuffers(env.window);
    //GL_DUMP_ERROR("draw loop");
    if (env.inputHandler.keys[GLFW_KEY_ESCAPE]) {
      glfwSetWindowShouldClose(env.window, 1);
    }
  }
}
