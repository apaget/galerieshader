#pragma once
#include <sys/stat.h>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <string>
#include "env.hpp"
#include "renderer.hpp"

enum class ShaderType { NORMAL, RAYMARCHING, RENDERBUFFER, BILLBOARD };

struct ShaderFile {
  std::string filename;
  std::time_t last_modification;
};

class RenderTarget;

class Shader {
 public:
  Shader(ShaderType type, std::string vertFilename, std::string fragFilename);
  Shader(ShaderType type, std::string fragFilename);
  Shader(Shader const &src);
  ~Shader(void);
  Shader &operator=(Shader const &rhs);

  GLuint id;
  ShaderType type;
  RenderTarget *renderTarget;
  std::vector<Shader *> dependencies;
  void use() const;
  void reload();

 private:
  Shader(void);
  GLuint loadVertex(std::string filename);
  GLuint loadFragment(std::string filename);
  GLuint compileShader(const std::string source, GLuint shaderType);
  GLuint linkShaders(GLuint vertexID, GLuint fragID);
  const std::string getShaderSource(std::string filename);
  void processRenderBuffers(const std::string &source);
  ShaderFile _vertex;
  ShaderFile _fragment;
};

void printShaderError(GLuint shade, std::string filename);
void printLinkError(GLuint program, std::string vextexFilename,
                    std::string fragmentFilename);
std::time_t getLastModificationTime(std::string filename);

const std::string vertex_shader_raymarching =
    "#version 410 core\n"
    "layout(location = 0) in vec3 vertex_pos;\n"
    "layout(location = 1) in vec2 vertex_uv;\n"
    "layout(location = 2) in vec3 vertex_normal;\n"
    "uniform mat4 MVP;\n"
    "uniform mat4 M;\n"
    "uniform mat4 V;\n"
    "uniform mat4 P;\n"
    "uniform vec3 iResolution;\n"
    "uniform float fovYscale;\n"
    "out vec2 fragCoord;\n"
    "out vec3 eye;\n"
    "out vec3 dir;\n"
    "out vec3 cameraForward;\n"
    "void main() {\n"
    " float aspect = iResolution.x / iResolution.y;\n"
    " gl_Position = vec4(vertex_pos, 1.0);\n"
    " mat4 MV = V * M;\n"
    " eye = -(MV[3].xyz) * mat3(MV);\n"
    " dir = vec3(vertex_pos.x * fovYscale * aspect,"
    "   vertex_pos.y * fovYscale, -1.0) * mat3(MV);\n"
    " cameraForward = vec3(0.0, 0.0, -1.0) * mat3(MV);"
    " fragCoord = vertex_uv * iResolution.xy;\n"
    "}\n";

const std::string vertex_shader_renderbuffer =
    "#version 410 core\n"
    "layout(location = 0) in vec3 vertex_pos;\n"
    "layout(location = 1) in vec2 vertex_uv;\n"
    "layout(location = 2) in vec3 vertex_normal;\n"
    "uniform mat4 MVP;\n"
    "uniform mat4 M;\n"
    "uniform mat4 V;\n"
    "uniform mat4 P;\n"
    "uniform vec3 iResolution;\n"
    "out vec2 fragCoord;\n"
    "void main() {\n"
    " gl_Position = vec4(vertex_pos, 1.0);\n"
    " fragCoord = vertex_uv * iResolution.xy;\n"
    "}\n";

const std::string vertex_shader_source =
    "#version 410 core\n"
    "layout(location = 0) in vec3 vertex_pos;\n"
    "layout(location = 1) in vec2 vertex_uv;\n"
    "layout(location = 2) in vec3 vertex_normal;\n"
    "uniform mat4 MVP;\n"
    "uniform mat4 M;\n"
    "uniform mat4 V;\n"
    "uniform mat4 P;\n"
    "uniform vec3 iResolution;\n"
    "out vec2 fragCoord;\n"
    "out vec3 eye;\n"
    "out vec3 dir;\n"
    "out vec3 cameraForward;\n"
    "void main() {\n"
    " gl_Position = MVP * vec4(vertex_pos, 1.0);\n"
    " fragCoord = vertex_uv * iResolution.xy;\n"
    "}\n";

const std::string frag_shader_renderbuffer =
    "#version 410 core\n"
    "layout(location = 0) out vec4 fragColor;\n"
    "in vec2 fragCoord;\n"
    "uniform sampler2D iChannel0;\n"
    "uniform sampler2D iChannel1;\n"
    "uniform sampler2D iChannel2;\n"
    "uniform sampler2D iChannel3;\n"
    "uniform mat4 MVP;\n"
    "uniform mat4 M;\n"
    "uniform mat4 V;\n"
    "uniform mat4 P;\n"
    "uniform vec3 camPos;\n"
    "uniform vec3 camDir;\n"
    "uniform vec3 camUp;\n"
    "uniform vec3 camRight;\n"
    "uniform vec3 billboardPos;\n"
    "uniform vec3 iResolution;\n"
    "uniform float iTime;\n"
    "uniform float iTimeDelta;\n"
    "uniform float iFrame;\n"
    "uniform float iChannelTime[4];\n"
    "uniform vec4 iMouse;\n"
    "uniform vec4 iDate;\n"
    "uniform float iSampleRate;\n"
    "uniform vec3 iChannelResolution[4];\n"
    "uniform float zNear;\n"
    "uniform float zFar;\n"
    "void mainImage(out vec4 fragColor, in vec2 fragCoord);\n"
    "void main() {\n"
    " mainImage(fragColor, fragCoord);\n"
    "}\n";

const std::string frag_shader_source =
    "#version 410 core\n"
    "in vec2 fragCoord;\n"
    "in vec3 eye;\n"
    "in vec3 dir;\n"
    "in vec3 cameraForward;\n"
    "out vec4 fragColor;\n"
    "uniform sampler2D iChannel0;\n"
    "uniform sampler2D iChannel1;\n"
    "uniform sampler2D iChannel2;\n"
    "uniform sampler2D iChannel3;\n"
    "uniform mat4 MVP;\n"
    "uniform mat4 M;\n"
    "uniform mat4 V;\n"
    "uniform mat4 P;\n"
    "uniform vec3 camPos;\n"
    "uniform vec3 camDir;\n"
    "uniform vec3 camUp;\n"
    "uniform vec3 camRight;\n"
    "uniform vec3 billboardPos;\n"
    "uniform vec3 iResolution;\n"
    "uniform float iTime;\n"
    "uniform float iTimeDelta;\n"
    "uniform float iFrame;\n"
    "uniform float iChannelTime[4];\n"
    "uniform vec4 iMouse;\n"
    "uniform vec4 iDate;\n"
    "uniform float iSampleRate;\n"
    "uniform vec3 iChannelResolution[4];\n"
    "uniform float zNear;\n"
    "uniform float zFar;\n"
    "void mainImage(out vec4 fragColor, in vec2 fragCoord);\n"
    "void main() {\n"
    " mainImage(fragColor, fragCoord);\n"
    "}\n";
