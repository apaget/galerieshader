#pragma once
#include <algorithm>
#include <map>
#include <vector>
#include "env.hpp"
#include "pixels.hpp"
#include "shader.hpp"

struct Texture {
  Texture(int width, int height);  // FBO
  Texture(std::string filename);
  ~Texture();
  std::string filename;  // Filename act as a key identifier
  GLuint id;
  int height;
  int width;
};

struct RenderTarget {
  RenderTarget(void);
  RenderTarget(std::string key, int width, int height);
  ~RenderTarget();
  std::string key;
  GLuint fbo_id;
  GLuint texture_channel;
  int width;
  int height;
  Texture* texture;
};

struct Uniforms {
  glm::mat4 view;
  glm::mat4 proj;
  GLint location;
  glm::vec3 iResolution;
  float iTime;
  float iTimeDelta;
  float iFrame;
  float iChannelTime[4];
  glm::vec4 iMouse;
  glm::vec4 iDate;
  float iSampleRate;
  glm::vec3 iChannelResolution[4];
  glm::vec3 camPos;
  glm::vec3 camDir;
  glm::vec3 camUp;
  glm::vec3 camRight;
  float zFar;
  float zNear;
  float fovYscale;
  /* samplerXX iChanneli; */
};

struct VAO {
  VAO(std::vector<Vertex> vertices);
  VAO(std::vector<glm::vec3> positions);
  ~VAO();
  GLuint vao;
  GLsizei vertices_size;
  GLsizei indices_size;
  std::vector<Vertex> vertices;

 private:
  GLuint _vbo;
};

class Shader;

struct RenderAttrib {
  VAO* vao;
  Shader* shader;
  std::vector<glm::mat4> transforms;
  glm::mat4 billboard;
  glm::vec3 billboardPos;
  Texture* iChannel0;
  Texture* iChannel1;
  Texture* iChannel2;
  Texture* iChannel3;

  bool operator<(const struct RenderAttrib& rhs) const;
};

class Renderer {
 public:
  Renderer(int width, int height);
  Renderer(Renderer const& src);
  virtual ~Renderer(void);
  Renderer& operator=(Renderer const& rhs);
  void addRenderAttrib(const RenderAttrib& renderAttrib);
  void draw();
  void flush();
  void reset();
  void printRenderAttribs();
  int getScreenWidth();
  int getScreenHeight();
  void loadCubeMap(std::string vertex_sha, std::string fragment_sha,
                   std::vector<std::string> textures);
  Uniforms uniforms;

 private:
  int _width;
  int _height;
  GLuint _cubeMap;
  VAO* _cubeMapVao;
  Shader* _cubeMapShader;
  Renderer(void);
  std::vector<RenderAttrib> _renderAttribs;
  void bindTexture(Texture* texture, int& texture_binded, GLenum tex_slot);
  void switchTextures(std::array<Texture*, 4> textures,
                      std::array<int, 4>& tex_channel);
  void switchShader(GLuint shader_id, int& current_shader_id);
  void updateUniforms(const RenderAttrib& attrib, const int shader_id,
                      std::array<int, 4>& tex_channel);
};

static inline void setUniform(const GLint& location, const float& data) {
  glUniform1f(location, data);
}
static inline void setUniform(const GLint& location, const int& data) {
  glUniform1i(location, data);
}
static inline void setUniform(const GLint& location, const glm::vec2& data) {
  glUniform2fv(location, 1, static_cast<const GLfloat*>(glm::value_ptr(data)));
}
static inline void setUniform(const GLint& location, const glm::vec3& data) {
  glUniform3fv(location, 1, static_cast<const GLfloat*>(glm::value_ptr(data)));
}
static inline void setUniform(const GLint& location, const glm::vec4& data) {
  glUniform4fv(location, 1, static_cast<const GLfloat*>(glm::value_ptr(data)));
}
static inline void setUniform(const GLint& location, const glm::mat4& data) {
  glUniformMatrix4fv(location, 1, GL_FALSE,
                     static_cast<const GLfloat*>(glm::value_ptr(data)));
}
