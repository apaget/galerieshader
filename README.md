# ShaderPixel

<img src="screenshots/mengersponge.png" width="300">
<img src="screenshots/mandelbox.png" width="300">
<img src="screenshots/mandeloctahedron.png" width="280">


Intro
-----
Shader museum combining raymarching/raytracing with traditional rasterization.

The shaders can be 2D/3D fractal or volumetric lighting.
Every shaders can be found under the [shaders](https://gitlab.com/apaget/galerieshader/tree/master/shaders) directory.  

Usage
-----
`./ShaderPixel`  
  
Use **WASD** to fly around and use the mouse to orient the camera.  

Build
-----
```
git clone --recursive https://gitlab.com/apaget/galerieshader
cd galerieshader
cmake . && make
```
